Page({
  onTap() {
    wx.createSelectorQuery()
      .select('#view')
      .boundingClientRect(res => {
        console.log('ID:', res.id)
        console.log('自定义属性:', res.dataset)
        console.log('宽高：', res.width, res.height)
        console.log('相对于视口的位置：', res.top, res.bottom, res.left, res.right)
      })
      .exec()
  }
})