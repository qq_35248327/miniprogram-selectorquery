Page({
  onTap() {
    // 分开获取：在步骤 3 添加回调函数，获取这次请求的结果
    const selectorQuery = wx.createSelectorQuery()
    selectorQuery.select('#view').boundingClientRect(res => {
      console.log('分开获取 view:', JSON.stringify(res))
    })
    selectorQuery.select('#view2').boundingClientRect(res => {
      console.log('分开获取 view2:', JSON.stringify(res))
    })
    selectorQuery.exec()
  },

  onTap2() {
    // 统一获取：在步骤 5 添加回调函数，获取所有请求的结果
    wx.createSelectorQuery()
      .select('#view').boundingClientRect()
      .select('#view2').boundingClientRect()
      .exec(res => {
        const viewRes = res[0]
        const view2Res = res[1]
        console.log('统一获取 view:', JSON.stringify(viewRes))
        console.log('统一获取 view2:', JSON.stringify(view2Res))
      })
  }
})