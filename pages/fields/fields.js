Page({
  onTap() {
    wx.createSelectorQuery()
      .select('#view')
      .fields(
        {
          id: true,
          dataset: true
        },
        res => {
          console.log('ID:', res.id)
          console.log('自定义属性:', res.dataset)
        }
      )
      .exec()
  }
})