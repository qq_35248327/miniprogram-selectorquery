Page({
  onTap() {
    wx.createSelectorQuery()
      .select('#canvas')
      .node(res => {
        const node = res.node
        console.log('Node: ', node)

        const context = node.getContext('2d')
        context.fillRect(0, 0, 100, 100)
      })
      .exec()
  }
})