Page({
  onTap() {
    // 1. 创建 SelectorQuery 对象
    let selectorQuery = wx.createSelectorQuery()
    // 2. 选择节点，得到 NodesRef 对象
    const nodesRef = selectorQuery.selectViewport()
    // 3. 添加查询节点信息的请求，重新得到 SelectorQuery 对象
    selectorQuery = nodesRef.boundingClientRect()
    // 4. 选择其他节点，添加其他查询节点信息的请求
    // ……
    // 5. 执行
    selectorQuery.exec(res => {
      res = res[0]
      console.log('宽高：', res.width, res.height)
      // 视口相对于视口的位置 (´▽｀)ノ♪
      console.log('相对于视口的位置：', res.top, res.bottom, res.left, res.right)
    })
  }
})