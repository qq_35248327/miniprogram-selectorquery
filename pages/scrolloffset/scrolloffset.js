Page({
  onTap() {
    wx.createSelectorQuery()
      .selectViewport()
      .scrollOffset(res => {
        console.log('ID:', res.id)
        console.log('自定义属性:', res.dataset)
        console.log('滚动位置：', res.scrollTop, res.scrollLeft)
      })
      .exec()
  }
})